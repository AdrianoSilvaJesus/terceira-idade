//importando React
import React from 'react';
//Importando React-Router
import { BrowserRouter as Router, Route} from "react-router-dom";

//Importando componente Menu
import Menu from './menu/menu';
//Importando componente Tela Inicial
import Inicio from './inicio/inicio';
//Importando componente Sobre
import Sobre from './sobre/sobre';

class App extends React.Component{
  render(){
    return(
	 	<Router>
	 		<Menu isLogged={false}/>
		    <Route path="/" exact component={Inicio}/>
		    <Route path="/sobre" component={Sobre}/>
	    </Router>
    );
  }
}

export default App;
