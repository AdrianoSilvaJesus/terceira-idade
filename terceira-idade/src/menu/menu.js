//Importando React
import React from 'react';

//Importando CSS do componente Menu
import './menu.css';

//Importando link's do Menu
import {Link} from "react-router-dom";

function Menu(props){
	if(props.isLogged === true){
		return(
			<h1>Deslogado</h1>
		);
	}else{
		return(
			<nav className="navbar navbar-expand-lg">
			  <a className="navbar-brand" href="#">Terceira Idade</a>
			  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span className="navbar-toggler-icon"></span>
			  </button>

			  <div className="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul className="navbar-nav mr-auto">
			      <li className="nav-item active">
			        <Link className="nav-link" to="/">Inicio <span className="sr-only">(current)</span></Link>
			      </li>
			      <li className="nav-item">
			        <Link className="nav-link" to="/sobre">Sobre</Link>
			      </li>
			      <li className="nav-item dropdown">
			        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			          Serviços
			        </a>
			        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
			          <a className="dropdown-item" href="#">Cuidador de Idosos</a>
			          <a className="dropdown-item" href="#">Treinador</a>
			          <a className="dropdown-item" href="#">Instrutor</a>
			          <div className="dropdown-divider"></div>
			          <a className="dropdown-item" href="#">Mais...</a>
			        </div>
			      </li>
			    </ul>
			    <form className="form-inline my-2 my-lg-0">
			      <input className="form-control mr-sm-2" type="email" placeholder="E-mail" aria-label="Search"></input>
			      <input className="form-control mr-sm-2" type="password" placeholder="Senha" aria-label="Search"></input>
			      <button className="btn my-2 my-sm" type="submit">Login</button>
			      <button className="btn my-2 my-sm">Registre-se</button>
			    </form>
			  </div>
			</nav>
		)
	}
}

export default Menu;